module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#2a7ca8',
      },
      fontFamily: {
        'body': ["'Montserrat', sans-serif"],
        'badge': ["'Roboto', sans-serif"],
        'headings': ["'Roboto', sans-serif"]
      },
    },
  },
  plugins: [
    require('tailwind-scrollbar'),
    require('@tailwindcss/forms'),
  ],
}
