import React from "react";
import { useState } from "react";
import { Outlet, useParams, NavLink } from 'react-router-dom';
import { DateTime } from "luxon";
import { ImCheckboxUnchecked, ImCheckboxChecked } from 'react-icons/im'

import { ToolBar, Button, Block } from "../../common/ui";
import { useGetStaffDetailsQuery, useGetStaffLogsQuery, useGetStaffQuery, useSignInOutStaffMutation } from "../../services";

const SideBarToolBar = ({ children }) => (
  <>
    {children}
  </>
)

const SideBar = ({ children }) => {
  const toolbar = [];
  const otherChildren = []

  React.Children.forEach(children, (child) => {
    if (child.type === SideBarToolBar) {
      toolbar.push(child);
    } else {
      otherChildren.push(child);
    };
  })

  return (
    <div className="w-3/12 h-full flex flex-col bg-gray-200 border-r border-slate-300 shrink-0">
      <ToolBar>
        {toolbar}
      </ToolBar>
      <div className="w-full grow overflow-y-scroll overflow-x-hidden scrollbar-thin scrollbar-thumb-slate-500 ">
        {otherChildren}
      </div>
    </div>
  )
}


const SideBarItem = ({ name, group, status, value }) => {

  const [ checked, setChecked ] = useState(false);

  const checkClicked = () => {
    setChecked(!checked)
  }

  return (
    <NavLink to={`/staff/${value}`} className={({isActive}) => (`w-full py-2 flex items-stretch border-b border-slate-300 ${isActive ? "bg-primary text-gray-100" : ""}`)}>
      <div onClick={checkClicked} className="w-10 flex items-center justify-center">
        {
          checked ?
          <ImCheckboxChecked /> :
          <ImCheckboxUnchecked />
        }
      </div>
      <div className="h-full grow flex items-center">
        <div className="grow">
          <p className="font-semibold">{name}</p>
          <p className="-mt-1 text-sm">{group}</p>
        </div>
        <div className="shrink-0 pr-4">
          <p className={`w-12 py-1 text-center rounded-3xl font-badge uppercase text-sm align-text-middle text-gray-800 ${ status === "in" ? "bg-sky-400" : " bg-amber-400"}`}>{status}</p>
        </div>
      </div>
      <div>
      </div>
    </NavLink>
  )
}

const StaffSideBar = () => {
  const [ search, setSearch ] = useState("");
  const { data=[], isLoading } = useGetStaffQuery({ search });

  let staff = (
    <h1>Loading</h1>
  )
  if (!isLoading) {
    staff = data.map((s) =>
      <SideBarItem name={`${s.firstName} ${s.lastName}`} group={s.group} status={s.status} value={s.id} key={s.id} />
    );
  }

  return (
    <SideBar>
      <SideBarToolBar>
        <input className="w-full" type="text" placeholder="Search" onChange={(event) => { setSearch(event.target.value) }} value={search} />
      </SideBarToolBar>
      {staff}
    </SideBar>
  )
}

const StaffLogs = ({ staffId }) => {
  const [ dateFrom, setDateFrom ] = useState(DateTime.local().minus({ month: 1 }).toISODate());
  const [ dateTo, setDateTo ] = useState(DateTime.local().toISODate());

  const { data=[], isLoading } = useGetStaffLogsQuery({ id: staffId, dateFrom, dateTo });

  let logs = (
    <h1>Loading</h1>
  )
  if (!isLoading) {
    logs = data.logs.map((l, index) => {

      let dates = l.dates.map((date, index) => {

        let times = date.times.map((time, index) => {
          let timeOut
          if (time.timeOut) {
            timeOut = DateTime.fromISO(time.timeOut).toFormat("HH:mm")
          }

          return (
            <li key={index} className="flex justify-between pb-1 pl-3">
              <span>
                {DateTime.fromISO(time.timeIn).toFormat("HH:mm")} <span className="italic text-xs px-3">to</span> {timeOut}
              </span>
              <span>
                {time.total}
              </span>
            </li>
          )
        })

        return (
        <li key={index} className="pb-5 pt-3">
          <div className="flex justify-between border-b border-gray-400">
            <span>{DateTime.fromISO(date.date).toFormat("dd LLL yyyy")}</span>
            <span>{date.total}</span>
          </div>
          <ul>
            {times}
          </ul>
        </li>
      )})

      return (
        <Block key={index} title={(
          <>
            <span>WB: {DateTime.fromISO(l.weekBegining).toFormat("dd LLL yyyy")}</span>
            <span>{l.total}</span>
          </>

        )}>

          <ul className="px-3 mb-3">
            {dates}
          </ul>
        </Block>
      )
    });
  }


  return (
    <div className="w-1/2 flex flex-col">
      <ToolBar>
        <label className="px-3" htmlFor="filter-from">from:</label>
        <input type="date" id="filter-from" onChange={(event) => { setDateFrom(event.target.value) }} value={dateFrom} />
        <label className="px-3" htmlFor="filter-to">to:</label>
        <input  type="date" id="filter-to" onChange={(event) => { setDateTo(event.target.value) }} value={dateTo} />
      </ToolBar>

      <div className="bg-gray-200 shadow-inner h-full overflow-y-scroll overflow-x-hidden px-5">
        {logs}
      </div>

    </div>
  )
}

export const StaffDetails = () => {
  let { id } = useParams()
  id = parseInt(id)
  const { data={}, isLoading } = useGetStaffDetailsQuery(id);
  const [ signInOut ] = useSignInOutStaffMutation();

  const DetailField = ({ title ,children }) => (
    <div className="px-0 my-5 border-b">
      <p className="uppercase tracking-wider text-[0.65em] font-medium text-gray-400">{title}</p>
      <p className="align-text-bottom pl-2">{children}</p>
    </div>
  )

  let details = (
    <h1>Loading</h1>
  )

  let notes

  if (!isLoading) {
    details = (
      <div className="bg-gray-100 overflow-hidden p-4 shrink-0 grid grid-cols-3 gap-x-5">
        <DetailField title="Title">{data.title}</DetailField>
        <DetailField title="First Name">{data.firstName}</DetailField>
        <DetailField title="Last Name">{data.lastName}</DetailField>
        <DetailField title="Date of Birth">{DateTime.fromISO(data.dateOfBirth).toFormat("dd LLL yyyy")}</DetailField>
        <DetailField title="Group">{data.group}</DetailField>
        <DetailField title="Status">{data.active ? "Active" : "Disabled" }</DetailField>
      </div>
    )

    notes = data.notes
  }
  return (
    <>
      <div className="border-r border-slate-300 w-1/2 flex flex-col">
        {/* Details */}
        {details}

        <ToolBar>
          {/* Toolbar */}
          <Button onClick={() => { signInOut(data.id) }}>{data.status === "in" ? "Sign Out" : "Sign In"}</Button>
        </ToolBar>

        <div className="w-full h-full overflow-hidden flex">
          <textarea readOnly className="w-full resize-none rounded-none border-none focus:rounded-none" value={notes} />
        </div>

      </div>

      <StaffLogs staffId={id} />
    </>
  )
}



export const Staff = () => {
  return (
    <div className="flex w-full h-full">

      <StaffSideBar />

      <div className="bg-gray-100 grow flex">
        <Outlet />
      </div>

    </div>
  )
}
