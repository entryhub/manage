import React, { useState } from "react";
import { DateTime } from "luxon";
import { useGetVisitorsQuery } from "../../services";
import { Table, TableCell, TableHeader, TableHead, TableBody, ToolBar } from "../../common/ui";

export const Visitors = () => {
  const [ dateFrom, setDateFrom ] = useState(DateTime.local().minus({ month: 1 }).toISODate());
  const [ dateTo, setDateTo ] = useState(DateTime.local().toISODate());
  const [ search, setSearch ] = useState("");
  // TODO: Handle Search better, Use timeout to prevent spamming the server
  const { data=[], isLoading } = useGetVisitorsQuery({ dateFrom, dateTo, search }, { pollingInterval: 3000 });


  let visitors
  if (!isLoading) {
    visitors = data.map((v) => {
      const timeIn = DateTime.fromISO(v.dateTimeIn).toFormat("dd LLL yyyy HH:mm");

      let timeOut = ""
      if (v.dateTimeOut !== null) {
        timeOut = DateTime.fromISO(v.dateTimeOut).toFormat("dd LLL yyyy HH:mm");
      }

      return (
        <tr key={v.id}>
          <TableCell className="text-center">{v.id}</TableCell>
          <TableCell>{v.name}</TableCell>
          <TableCell>{v.company}</TableCell>
          <TableCell className="text-center">{v.carRegistration}</TableCell>
          <TableCell>{v.reason}</TableCell>
          <TableCell className="text-center">{timeIn}</TableCell>
          <TableCell className="text-center">{timeOut}</TableCell>
        </tr>
      )
    }

    );
  }

  return (

    <div className="flex flex-col w-full h-full">
      <ToolBar>
        <input type="text" placeholder="Search" onChange={(event) => { setSearch(event.target.value) }} value={search} />
        <div>
          <label className="px-3" htmlFor="filter-from">from:</label>
          <input type="date" id="filter-from" onChange={(event) => { setDateFrom(event.target.value) }} value={dateFrom} />
          <label className="px-3" htmlFor="filter-to">to:</label>
          <input  type="date" id="filter-to" onChange={(event) => { setDateTo(event.target.value) }} value={dateTo} />
        </div>
      </ToolBar>
      <div className="h-full w-full overflow-y-scroll overflow-x-hidden scrollbar-thin scrollbar-thumb-slate-500 ">
        <Table>
          <TableHead className="">
            <tr >
              <TableHeader className="w-10">#</TableHeader>
              <TableHeader className="w-80">Name</TableHeader>
              <TableHeader className="w-80">Company</TableHeader>
              <TableHeader className="w-36">Car Reg</TableHeader>
              <TableHeader>Reason</TableHeader>
              <TableHeader className="w-48">Time In</TableHeader>
              <TableHeader className="w-48">Time Out</TableHeader>
            </tr>
          </TableHead>
          <TableBody className="h-full">
            {visitors}
          </TableBody>
        </Table>

      </div>

    </div>
  )
}
