import React, { useState } from "react";
import { DateTime } from "luxon";
import { Table, TableCell, TableHeader, TableHead, TableBody, ToolBar } from "../../common/ui";
import { useGetLatePupilsQuery } from "../../services";

export const LatePupils = () => {
  const [ dateFrom, setDateFrom ] = useState(DateTime.local().toISODate());
  const [ dateTo, setDateTo ] = useState(DateTime.local().toISODate());
  const [ search, setSearch ] = useState("");
// TODO: Handle Search better, Use timeout to prevent spamming the server
  const { data=[], isLoading } = useGetLatePupilsQuery({ dateFrom, dateTo, search },{ pollingInterval: 3000 });

  let pupils = (
    <h1>Loading</h1>
  )
  if (!isLoading) {
    pupils = data.map((p) => {
      // TODO: Date time format in settings
      const timeIn = DateTime.fromISO(p.dateTimeIn).toFormat("dd LLL yyyy HH:mm");

      return (
        <tr key={p.id}>
          <TableCell className="text-center">{p.id}</TableCell>
          <TableCell>{p.name}</TableCell>
          <TableCell className="text-center">{p.className}</TableCell>
          <TableCell>{p.reason}</TableCell>
          <TableCell className="text-center">{timeIn}</TableCell>
        </tr>
      )
    }

    );
  }


  return (

    <div className="flex flex-col w-full h-full">
      <ToolBar>
        <input type="text" placeholder="Search" onChange={(event) => { setSearch(event.target.value) }} value={search} />
        <div>
          <label className="px-3" htmlFor="filter-from">from:</label>
          <input type="date" id="filter-from" onChange={(event) => { setDateFrom(event.target.value) }} value={dateFrom} />
          <label className="px-3" htmlFor="filter-to">to:</label>
          <input  type="date" id="filter-to" onChange={(event) => { setDateTo(event.target.value) }} value={dateTo} />
        </div>
      </ToolBar>
       <div className="h-full w-full overflow-y-scroll overflow-x-hidden scrollbar-thin scrollbar-thumb-slate-500 ">
        <Table>
          <TableHead>
            <tr>
              <TableHeader className="w-10">#</TableHeader>
              <TableHeader className="w-96">Name</TableHeader>
              <TableHeader className="w-24">Class</TableHeader>
              <TableHeader>Reason</TableHeader>
              <TableHeader className="w-52">Time In</TableHeader>
            </tr>
          </TableHead>
          <TableBody>
            {pupils}
          </TableBody>
        </Table>

      </div>

    </div>
  )
}