import React, { useRef } from "react";
import { useReactToPrint } from 'react-to-print';
import { ToolBar, Button, Block } from "../../common/ui";
import { useGetWhosInQuery } from "../../services";

export const WhosIn = () => {
  const { data=[], isLoading } = useGetWhosInQuery({}, { pollingInterval: 300000 });
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  const ListItem = ({ name, group }) => (
    <li className="flex justify-between mb-2 border-b border-gray-400">
      <span className="capitalize">
        {name}
      </span>
      <span className="capitalize">
        {group}
      </span>
    </li>
  )

  let staff
  let visitors
  let latePupils
  let printStaff
  let printVisitors
  let printLatePupils

  if (!isLoading) {
    staff = data.staff.map((s, index) => (
      <ListItem name={s.name} group={s.group} key={index} />
    ))

    visitors = data.visitors.map((v, index) => (
      <ListItem name={v.name} group={v.company} key={index} />
    ))

    latePupils = data.latePupils.map((p, index) => (
      <ListItem name={p.name} group={p.group} key={index} />
    ))

    printStaff = data.staff.map((s, index) => (
      <tr key={index}>
        <td className="px-1 py-1 border border-gray-700">{s.name}</td>
        <td className="px-1 py-1 border border-gray-700">{s.group}</td>
        <td className="px-1 py-1 border border-gray-700"></td>
      </tr>
    ))

    printVisitors = data.visitors.map((v, index) => (
      <tr key={index}>
        <td className="px-1 py-1 border border-gray-700">{v.name}</td>
        <td className="px-1 py-1 border border-gray-700">{v.company}</td>
        <td className="px-1 py-1 border border-gray-700"></td>
      </tr>
    ))

    printLatePupils = data.latePupils.map((p, index) => (
      <tr key={index}>
        <td className="px-1 py-1 border border-gray-700">{p.name}</td>
        <td className="px-1 py-1 border border-gray-700">{p.group}</td>
        <td className="px-1 py-1 border border-gray-700"></td>
      </tr>
    ))

  }

  return (
    <div className="bg-gray-100 flex flex-col h-full" >
      <ToolBar>
        <Button onClick={handlePrint}>Print</Button>
      </ToolBar>
      <div className="flex h-full items-stretch justify-items-stretch">

        <Block title="Staff">
          {staff}
        </Block>

        <Block title="Visitors">
          {visitors}
        </Block>

        <Block title="Late Pupils">
          {latePupils}
        </Block>

      </div>
      <div className="hidden">
        <div className="w-full h-full text-black" ref={componentRef}>

          <h1 className="text-center text-2xl">Staff</h1>

          <table className="w-full">
            <thead className="text-left">
              <tr >
                <th className="w-64 px-2 py-1">Name</th>
                <th className="px-2 py-1">Group</th>
                <th className="w-10 px-2 py-1">Tick</th>
              </tr>
            </thead>
            <tbody>
              {printStaff}
            </tbody>
          </table>

          <div class="pagebreak"> </div>

          <h1 className="text-center text-2xl">Visitors</h1>


          <table className="w-full">
            <thead className=" text-left">
              <tr >
                <th className="w-64 px-2 py-1">Name</th>
                <th className="px-2 py-1">Group</th>
                <th className="w-10 px-2 py-1">Tick</th>
              </tr>
            </thead>
            <tbody>
              {printVisitors}
            </tbody>
          </table>

          <div class="pagebreak"> </div>

          <h1 className="text-center text-2xl">Late Pupils</h1>
          <table className="w-full">
            <thead className=" text-left">
              <tr >
                <th className="w-64 px-2 py-1">Name</th>
                <th className="px-2 py-1">Group</th>
                <th className="w-10 px-2 py-1">Tick</th>
              </tr>
            </thead>
            <tbody>
              {printLatePupils}
            </tbody>
          </table>
        </div>
      </div>

    </div>
  )
}