import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../../common/ui";

export const Settings = () => {
  const navigate = useNavigate();
  return (
    <div className="flex w-full h-full justify-center items-start">
      <div className=" rounded-md bg-gray-200 w-96 flex flex-col items-center mt-11 p-5 border border-gray-300 shadow-md">
        <h1 className="font-headings uppercase text-lg text-center m-2">Settings</h1>
        <p className="mb-2 text-center">The settings feature is currectly not available. This feature will be available in future releases</p>
        <Button onClick={() => { navigate("/") }} className="">Go to Homepage</Button>
      </div>
    </div>
  )
}