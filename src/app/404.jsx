import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../common/ui";

export const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className="flex w-full h-full justify-center items-start">
      <div className=" rounded-md bg-gray-200 w-96 flex flex-col items-center mt-11 p-5 border border-gray-300 shadow-md">
        <h1 className="font-headings uppercase text-lg text-center m-2">Oops! That page doesn't exist.</h1>
        <p className="mb-2 text-center">The link you clicked may be broken or the page may have been removed.</p>
        <Button onClick={() => { navigate("/") }} className="">Go to Homepage</Button>
      </div>
    </div>
  )
}