import React from "react";

export const Table = ({children}) => (
  <table className="relative w-full divide-y divide-gray-200">
    {children}
  </table>

)

export const TableHead = ({children}) => (
  <thead>
    {children}
  </thead>

)


export const TableHeader = ({children, className}) => (
  <th scope="col" className={`sticky top-0 py-4 px-3 text-left text-xs font-light font-headings text-gray-700 bg-gray-300 uppercase tracking-wider  ${className}`}>
    {children}
  </th>

)

export const TableBody = ({children}) => (
  <tbody className="bg-gray-100 divide-y divide-gray-200 w-full h-[100px]">
    {children}
  </tbody>
)

export const TableCell = ({children, className}) => (
  <td className={`px-2 py-3 border ${className}`}>
    {children}
  </td>
)

export const Textbox = ({children}) => (
  <input className="" type="text" placeholder="Search" />
)

export const ToolBar = ({ children, className }) => (
  <div className={`w-full h-12 bg-primary text-gray-100 flex justify-start items-center px-2 shrink-0 ${className}`}>
    {children}
  </div>
)

export const Button = ({ children, className, onClick }) => (
  <button onClick={onClick} className={`px-4 py-1.5 m-1 rounded-lg bg-teal-500 font-headings uppercase text-sm ${className}`}>
    {children}
  </button>
)

export const Block = ({ title, children }) => (
    <div className="bg-gray-300 my-3 mx-1.5 rounded-lg border border-gray-400 w-full flex flex-col">
      <div className="rounded-t-lg bg-primary text-gray-200 -m-[1px] p-2 font-light font-headings tracking-wider flex justify-between">
        {title}
      </div>
      <ul className="px-3 m-1 h-full overflow-hidden overflow-y-scroll">
        {children}
      </ul>
    </div>
  )