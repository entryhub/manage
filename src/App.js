import React from 'react';
import { BrowserRouter, Route, Routes, NavLink, Navigate } from 'react-router-dom';
import { FaChild, FaRegEye } from 'react-icons/fa';
import { BsPersonBadgeFill, BsPeopleFill } from 'react-icons/bs';

import { Visitors } from './features/visitors/Visitors';
import { Staff, StaffDetails } from './features/staff/Staff';
import { LatePupils } from './features/latePupils/LatePupils';
import { WhosIn } from './features/whosin/WhosIn';
import { NotFound } from './app/404';
import { Settings } from './features/settings/setting';


const NavButton = ({icon, to, tooltip}) => (
  <NavLink to={to} className={({isActive}) => (
    `bg-slate-800 h-10 w-10 hover:rounded-lg text-white text-xl mb-2 flex items-center justify-center cursor-pointer shadow-lg group ${isActive ? "rounded-lg bg-primary" : "rounded-full"}`)}>
    {icon}
    <span className="absolute w-auto p-2 m-2 min-w-max left-14 rounded-md shadow-md
    text-white bg-gray-900 font-headings uppercase text-xs
    transition-all duration-200 scale-0 origin-left group-hover:scale-100 z-50">{tooltip}</span>
  </NavLink>
)

//const Divider = () => <hr className="bg-gray-200 dark:bg-gray-800 border border-gray-200 dark:border-gray-800 rounded-full mb-3 mt-1 w-3/6" />;

function App() {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="flex flex-col h-screen w-screen">
        <div className="grow flex h-[100px] w-full">
          <nav className="h-full w-14 bg-slate-700 flex flex-col items-center justify-start pt-5">
            <NavButton to="/visitors" icon={<BsPeopleFill />} tooltip="Visitors" />
            <NavButton to="/staff" icon={<BsPersonBadgeFill />} tooltip="Staff" />
            <NavButton to="/latepupils" icon={<FaChild />} tooltip="Late Pupils" />
            <NavButton to="/whosin" icon={<FaRegEye />} tooltip="Who's In" />
            {/*
              <Divider />
              <NavButton to="/settings" icon={<FaCog />} tooltip="Settings" />
            */}
          </nav>
          <main className="grow overflow-hidden w-full h-full" >

            <Routes>
              <Route exact path="/" element={<Navigate to="/visitors" />} />
              <Route path="/visitors" element={<Visitors />} />
              <Route path="/staff/" element={<Staff />}>
                <Route path=":id" element={<StaffDetails />} />
              </Route>
              <Route exact path="/latepupils" element={<LatePupils />} />
              <Route exact path="/whosin" element={<WhosIn />} />
              <Route exact path="/settings" element={<Settings />} />
              <Route path="*" element={<NotFound />} />
            </Routes>

          </main>
        </div>

        <footer className="bg-primary h-7 w-full p-1 text-xs font-headings font-light uppercase text-gray-100 flex items-center justify-between">
          <p>Powered By Emmet</p>
          <p></p>
        </footer>
    </div>
    </BrowserRouter>

  );
}

export default App;
