# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1]

### Fixed
 - Search bar location on staff page.

## [0.1.0] - 2022-02-13

### Added
- Initial Release.


[unreleased]: https://gitlab.com/entryhub/manage/-/compare/main...0.1.1
[0.1.1]: https://gitlab.com/entryhub/manage/-/compare/0.1.1...0.1.0
[0.1.0]: https://gitlab.com/entryhub/manage/-/tags/0.1.0